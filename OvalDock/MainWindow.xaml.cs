﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Automation;

using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Interop;
using System.Windows.Media;
using Application = System.Windows.Application;
using Button = System.Windows.Controls.Button;
using DataFormats = System.Windows.DataFormats;
using DragEventArgs = System.Windows.DragEventArgs;
using HorizontalAlignment = System.Windows.HorizontalAlignment;
using Image = System.Windows.Controls.Image;
using Label = System.Windows.Controls.Label;
using MessageBox = System.Windows.MessageBox;


namespace OvalDock
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Code for global hotkeys
        // https://social.technet.microsoft.com/wiki/contents/articles/30568.wpf-implementing-global-hot-keys.aspx
        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private HwndSource source;
        private IntPtr handle;
        private const int HOTKEY_ID = 9000;
        // End of hotkey code
        public System.Windows.Controls.Image InnerDisk { get; private set; }
        public Button InnerDiskButton { get; private set; }
        public System.Windows.Controls.Image OuterDisk { get; private set; }

        // TODO: Probably more hassle than it's worth, but should this second argument be the inner circle icon somehow?
        public PieFolderItem RootFolder { get; private set; }
        public PieFolderItem CurrentFolder { get; private set; }

        //public List<Button> ItemButtons { get; }
        //public List<KeyValuePair<Button, Label>> ItemLabels { get; }

        List<ItemDisplayInfo> ItemDisplayInfos { get; }

        // The context menu to be shared between the inner and outer disks.
        // Essentially, we create it once, then cache it forever.
        // Should only ever call this using the property, not the raw value.
        private ContextMenu _diskContextMenu = null;
        private ContextMenu DiskContextMenu
        {
            get
            {
                if (_diskContextMenu == null)
                {
                    _diskContextMenu = new ContextMenu();

                    // TODO: There is a little bit of code duplication between this and the NotifyIcon (Windows Forms)
                    //       Sort it out at some point.
                    
                    /*
                     
                    MenuItem addMenuItem = new MenuItem();
                    addMenuItem.Header = "Add item";
                    addMenuItem.Click +=
                        (s, e) =>
                        {
                            // TODO: This feels inefficient, but PieItemSettingsWindow(null, ...) feels like something might break
                            PieItem newItem = new FileItem(false, null, false, null, null, null, FileItemType.File);
                            PieItemSettingsWindow pieItemSettingsWindow = new PieItemSettingsWindow(newItem, CurrentFolder);
                            pieItemSettingsWindow.ShowDialog();

                            if (pieItemSettingsWindow.Saved)
                            {
                                CurrentFolder.Items.Add(pieItemSettingsWindow.NewPieItem);
                                RefreshFolder();
                                Config.SaveItems(RootFolder);
                            }
                        };

                    _diskContextMenu.Items.Add(addMenuItem);
                     
                     */
                    
                    MenuItem programSettingsMenuItem = new MenuItem();
                    programSettingsMenuItem.Header = "Paramètres";
                    programSettingsMenuItem.Click +=
                        (s, e) =>
                        {
                            if (ProgramSettingsWindow.IsWindowActive)
                            {
                                MessageBox.Show("La fenêtre des paramètres est déjà ouverte");
                                return;
                            }
                            ProgramSettingsWindow settingsWindow = new ProgramSettingsWindow(this);
                            settingsWindow.Show();
                        };


                    MenuItem refreshMenuItem = new MenuItem();
                    refreshMenuItem.Header = "Rafraîchir";
                    refreshMenuItem.Click +=
                        (s, e) =>
                        {
                            CurrentFolder = RootFolder;
                            RefreshFoldersList();
                            RefreshFolder();
                        };
                    
                    MenuItem resetMenuItem = new MenuItem();
                    resetMenuItem.Header = "Réinitialiser";
                    resetMenuItem.Click +=
                        (s, e) =>
                        {
                            CurrentFolder = RootFolder;
                            ResetFoldersList();
                            RefreshFolder();
                        };

                    MenuItem hideMenuItem = new MenuItem();
                    hideMenuItem.Header = "Masquer";
                    hideMenuItem.Click +=
                    (s, e) =>
                    {
                        if (IsVisible)
                            Hide();
                        else
                            ToggleVisibility();
                    };
                    


                    MenuItem quitMenuItem = new MenuItem();
                    quitMenuItem.Header = "Quitter";
                    quitMenuItem.Click +=
                    (s, e) =>
                    {
                        //Close(); // Close() only shuts down the current window. This doesn't work if there are other windows open.
                        Application.Current.Shutdown();
                    };
                    
                    Separator separator = new Separator();

                    _diskContextMenu.Items.Add(refreshMenuItem);
                    _diskContextMenu.Items.Add(programSettingsMenuItem);
                    _diskContextMenu.Items.Add(hideMenuItem);
                    _diskContextMenu.Items.Add(quitMenuItem);
                    _diskContextMenu.Items.Add(separator);
                    _diskContextMenu.Items.Add(resetMenuItem);
                }

                return _diskContextMenu;
            }
        }



        // Unfortunately have to rely on Windows Forms for this.
        // .csproj was modified to use Windows Forms as well.
        private System.Windows.Forms.NotifyIcon notifyIcon = new System.Windows.Forms.NotifyIcon();

        private static void CreateDirCheckPerm(string path, string element)
        {
            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch
                {
                    MessageBox.Show("OvalDock n'a pas les permissions d'écriture sur " + element + ", cela peut engendrer des défauts de fonctionnement");
                }
            }
        }
        
        public MainWindow()
        {
            InitializeComponent();
            Topmost = false;

            // TODO: Organize this better? Or rename LoadConfig() to LoadProgramConfig()?
            Config.LoadProgramSettings();
            //RootFolder = Config.LoadItems();

            {
                string[] args = Environment.GetCommandLineArgs();
                if (args.Length >= 2)
                {
                    if (args[1] == "--check-startup-config" && Config.UserConsent == "false")
                    {
                        Application.Current.Shutdown();
                    }
                }
            }
            
            CreateDirCheckPerm(Config.ProtectedDirLocation, "le dossier protégé");
            
            RootFolder = new PieFolderItem(false, null, true, Config.InnerDiskImagePath, null, "Root");
            CurrentFolder = RootFolder;

            RefreshFoldersList();
            
            ItemDisplayInfos = new List<ItemDisplayInfo>();

            //ItemButtons = new List<Button>();
            //ItemLabels = new List<KeyValuePair<Button, Label>>();

            ResizeWindow();

            CreateOuterDisk();
            CreateInnerDisk();

            ResetPosition();
            
            CreateNotifyIcon();

            PreloadIconsRecursive(RootFolder);
            RefreshFolder();
            ToggleVisibility();
        }

        private void ResetFoldersList()
        {
            try
            {
                if (MessageBox.Show("Êtes vous sûr de vouloir réinitialiser tous vos raccourcis ?", "Réinitialiser", MessageBoxButton.YesNo) == MessageBoxResult.No)
                    return;
                Directory.Delete(Config.CustomDirLocation, true);
            }
            catch (Exception)
            {
                //MessageBox.Show(e.Message);
            }
            
            RefreshFoldersList();
        }
        
        private void RefreshFoldersList()
        {
            try
            {
                RootFolder.Items.Clear();
                DuplicateToCustom(Config.ProtectedDirLocation, Config.CustomDirLocation);
                if (!Config.DirectoryHasWriteAccess(Config.CustomDirLocation))
                {
                    MessageBox.Show("OvalDock n'a pas les permissions d'écriture sur le dossier utilisateur, cela peut engendrer des défauts de fonctionnement");
                }
                ParseFolders(Config.ProtectedDirLocation);
                CurrentFolder = RootFolder;
                ParseFolders(Config.CustomDirLocation, false);
                CurrentFolder = RootFolder;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void DuplicateToCustom(string src, string dest)
        {
            foreach (var d in Directory.GetDirectories(src))
            {
                string newPath = Path.Combine(dest, Path.GetRelativePath(src, d));
                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                DuplicateToCustom(d, newPath);
            }
        }

        private void AddFolderFiles(string d, bool isProtected = true)
        {
            foreach (var f in Directory.GetFiles(d))
            {
                if (File.GetAttributes(f).HasFlag(FileAttributes.Hidden) || Path.GetFileName(f).StartsWith("."))
                    continue;
                if (CurrentFolder.Items.Any(item => item.Name == Path.GetFileNameWithoutExtension(f)))
                    continue;
                PieItem newItem = new FileItem(true, Path.GetFileNameWithoutExtension(f), false, null, Path.GetFullPath(f), null, FileItemType.File, isProtected);
                CurrentFolder.Items.Add(newItem);
            }
        }

        private void AddFolder(string dir, bool isProtected = true)
        {
            PieFolderItem oldFolder = CurrentFolder;
            PieFolderItem newFolder;

            if (Path.GetFileName(dir).StartsWith(".")) return;

            try
            {
                newFolder = (PieFolderItem)CurrentFolder.Items.First(item => item.TypeName == "PieFolderItem" && item.Name == Path.GetFileName(dir));
            }
            catch
            {
                newFolder = new PieFolderItem(true, Path.GetFileName(dir), false , null, CurrentFolder, Path.GetFullPath(dir), isProtected);
                CurrentFolder.Items.Add(newFolder);
            }
            CurrentFolder = newFolder;
            ParseFolders(dir, isProtected);
            CurrentFolder = oldFolder;
        }
        
        private void ParseFolders(string dir, bool isProtected = true)
        {
            try
            {
                AddFolderFiles(dir, isProtected);
                foreach (var d in Directory.GetDirectories(dir))
                {
                    if (Path.GetFileName(d).StartsWith("."))
                        continue;
                    AddFolder(d, isProtected);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        protected void RegisterHotkeyOne()
        {
            Config.HotkeyModifiers = Win32Helper.MOD_CONTROL + Win32Helper.MOD_WIN + Win32Helper.MOD_SHIFT;
            if (RegisterHotKey(handle, HOTKEY_ID, Config.HotkeyModifiers, Win32Helper.VK_LWIN) == false)
            {
                MessageBox.Show(
                    "OvalDock n'a pas réussi a démarrer correctement. Assurez vous de ne pas avoir une autre instance d'OvalDock en cours d'exécution");
                Application.Current.Shutdown();
                return;
            }
            if (RegisterHotKey(handle, HOTKEY_ID + 1, Config.HotkeyModifiers, Win32Helper.VK_SHIFT) == false)
            {
                MessageBox.Show(
                    "OvalDock n'a pas réussi a démarrer correctement. Assurez vous de ne pas avoir une autre instance d'OvalDock en cours d'exécution");
                Application.Current.Shutdown();
                return;
            }
            if (RegisterHotKey(handle, HOTKEY_ID + 2, Config.HotkeyModifiers, Win32Helper.VK_CONTROL) == false)
            {
                MessageBox.Show(
                    "OvalDock n'a pas réussi a démarrer correctement. Assurez vous de ne pas avoir une autre instance d'OvalDock en cours d'exécution");
                Application.Current.Shutdown();
                return;
            }
        }

        protected void RegisterHotkeyTwo()
        {
            Config.HotkeyModifiers = Win32Helper.MOD_CONTROL + Win32Helper.MOD_WIN + Win32Helper.MOD_ALT;
            if (RegisterHotKey(handle, HOTKEY_ID, Config.HotkeyModifiers, Win32Helper.VK_LWIN) == false)
            {
                MessageBox.Show(
                    "OvalDock n'a pas réussi a démarrer correctement. Assurez vous de ne pas avoir une autre instance d'OvalDock en cours d'exécution");
                Application.Current.Shutdown();
                return;
            }
            if (RegisterHotKey(handle, HOTKEY_ID + 1, Config.HotkeyModifiers, Win32Helper.VK_MENU) == false)
            {
                MessageBox.Show(
                    "OvalDock n'a pas réussi a démarrer correctement. Assurez vous de ne pas avoir une autre instance d'OvalDock en cours d'exécution");
                Application.Current.Shutdown();
                return;
            }
            if (RegisterHotKey(handle, HOTKEY_ID + 2, Config.HotkeyModifiers, Win32Helper.VK_CONTROL) == false)
            {
                MessageBox.Show(
                    "OvalDock n'a pas réussi a démarrer correctement. Assurez vous de ne pas avoir une autre instance d'OvalDock en cours d'exécution");
                Application.Current.Shutdown();
                return;
            }
        }
        
        // Handle hotkey registration in here.
        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            handle = new WindowInteropHelper(this).Handle;
            source = HwndSource.FromHwnd(handle);
            source.AddHook(HwndHook);

            if (Config.AltHotkey)
                RegisterHotkeyTwo();
            else
                RegisterHotkeyOne();
        }

        // Also for handling hotkey
        private IntPtr HwndHook(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_HOTKEY = 0x0312;
            switch (msg)
            {
                case WM_HOTKEY:
                    int key = wParam.ToInt32();
                    if (key >= HOTKEY_ID && key <= HOTKEY_ID + 2) {
                        ToggleVisibility();
                        handled = true;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        // Preload the icons for speed
        public void PreloadIconsRecursive(PieFolderItem folder)
        {
            // Force each icon to load (at least) once.
            // Preloading is handled in the IconAsBitmapSource property.
            //
            // No need to worry about "preloading twice".
            // The second time, it has already been preloaded.
            folder.Icon.CreateCache();

            foreach (PieItem item in folder.Items)
            {
                item.Icon.CreateCache();
            }

            foreach (PieItem item in folder.Items)
            {
                if (item is PieFolderItem)
                {
                    PreloadIconsRecursive((PieFolderItem)item);
                }
            }
        }

        public void ResizeWindow()
        {
                              // Plenty of space, so that labels can go quite a bit out of "bounds"
            double maxRadius = Math.Max(Config.InnerRadius * Config.ScreenFactor, Config.OuterRadius * Config.ScreenFactor);

            mainWindow.Width = 4 * maxRadius;
            mainWindow.Height = 4 * maxRadius;

            mainGrid.Width = 4 * maxRadius;
            mainGrid.Height = 4 * maxRadius;
        }

        private void CreateNotifyIcon()
        {
            // TODO: Make a proper icon for this.
            notifyIcon.Icon = Properties.Resources.OvalDock_256x256;
            notifyIcon.Text = Config.ProgramName;
            notifyIcon.Visible = true;

            System.Windows.Forms.ToolStripMenuItem quitItem = new System.Windows.Forms.ToolStripMenuItem();
            quitItem.Text = "Quitter";
            quitItem.Click +=
                (s, e) =>
                {
                    //Close(); // Close() only shuts down the current window. This doesn't work if there are other windows open.
                    Application.Current.Shutdown();
                };

            System.Windows.Forms.ToolStripMenuItem settingsItem = new System.Windows.Forms.ToolStripMenuItem();
            settingsItem.Text = "Paramètres";
            settingsItem.Click +=
                (s, e) =>
                {
                    if(ProgramSettingsWindow.IsWindowActive)
                    {
                        MessageBox.Show("La fenêtre des paramètres est déjà ouverte");
                        return;
                    }
                    ProgramSettingsWindow settingsWindow = new ProgramSettingsWindow(this);
                    settingsWindow.Show();
                };
            
            System.Windows.Forms.ToolStripMenuItem hideMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            hideMenuItem.Text = "Masquer / Afficher";
            hideMenuItem.Click +=
                (s, e) =>
                {
                    if (IsVisible)
                        Hide();
                    else
                        ToggleVisibility();
                };


            System.Windows.Forms.ContextMenuStrip contextMenu = new System.Windows.Forms.ContextMenuStrip();

            contextMenu.Items.Add(settingsItem);
            contextMenu.Items.Add(hideMenuItem);
            contextMenu.Items.Add(quitItem);

            notifyIcon.ContextMenuStrip = contextMenu;
            notifyIcon.Click +=
                (s, e) =>
            {
                ToggleVisibility();
            };
        }

        public void ResetPosition()
        {
            Config.ComputeScreenFactor();
            InnerDisk.Width = 2 * Config.InnerRadius * Config.ScreenFactor;
            InnerDisk.Height = 2 * Config.InnerRadius * Config.ScreenFactor;
            if (RootFolder != CurrentFolder)
            {
                InnerDiskButton.Width = InnerDisk.Width + InnerDisk.Width / 4 ;
                InnerDiskButton.Height = InnerDisk.Height + InnerDisk.Height / 4 ;
            }
            OuterDisk.Width = 2 * Config.OuterRadius * Config.ScreenFactor;
            OuterDisk.Height = 2 * Config.OuterRadius * Config.ScreenFactor;
            ResizeWindow();
            UpdateItemAppearance();
            
            double widthModifier = Config.WindowPositionPercentageWidth / 100;
            double heightModifier = Config.WindowPositionPercentageHeight / 100;

            Left = SystemParameters.PrimaryScreenWidth * widthModifier - Width * widthModifier;;
            Top = SystemParameters.PrimaryScreenHeight * heightModifier - Height * heightModifier;
        }
        
        public void ToggleVisibility()
        {
            if (mainWindow.IsActive)
            {
                Hide();
                return;
            }
            ResetPosition();
            Show();
            Activate();
        }

        private void CreateOuterDisk()
        {
            OuterDisk = new System.Windows.Controls.Image();

            //outerDisk.Source = new BitmapImage(new Uri(outerDiskImagePath));
            OuterDisk.Source = Util.ToBitmapImage(new Bitmap(Config.OuterDiskImagePath));
            OuterDisk.Width = 2 * Config.OuterRadius * Config.ScreenFactor;
            OuterDisk.Height = 2 * Config.OuterRadius * Config.ScreenFactor;

            //var margin = new Thickness();
            //margin.Left = 0;
            //margin.Top = 0;
            //outerDisk.Margin = margin;

            OuterDisk.Opacity = Config.OuterDiskNormalOpacity;

            OuterDisk.HorizontalAlignment = HorizontalAlignment.Center;
            OuterDisk.VerticalAlignment = VerticalAlignment.Center;

            //outerDisk.MouseRightButtonUp += OuterDisk_MouseRightButtonUp;
            OuterDisk.MouseLeftButtonDown += OuterDisk_MouseLeftButtonDown;

            OuterDisk.ContextMenu = DiskContextMenu;

            OuterDisk.AllowDrop = true;
            OuterDisk.Drop += Disk_Drop;

            mainGrid.Children.Add(OuterDisk);
        }

        private void CreateInnerDisk()
        {
            InnerDisk = new System.Windows.Controls.Image();

            // innerDisk.Source = ToBitmapImage(new Bitmap(Config.InnerDiskImagePath));*
            InnerDisk.Source = CurrentFolder.Icon.ImageBitmapSource;

            InnerDisk.Opacity = Config.InnerDiskNormalOpacity;

            InnerDisk.Width = 2 * Config.InnerRadius * Config.ScreenFactor;
            InnerDisk.Height = 2 * Config.InnerRadius * Config.ScreenFactor;

            InnerDisk.HorizontalAlignment = HorizontalAlignment.Center;
            InnerDisk.VerticalAlignment = VerticalAlignment.Center;

            InnerDisk.ContextMenu = DiskContextMenu;

            InnerDisk.AllowDrop = true;
            InnerDisk.Drop += Disk_Drop;

            InnerDiskButton = new Button
            {
                Width = InnerDisk.Width + InnerDisk.Width / 4,
                Height = InnerDisk.Height + InnerDisk.Height / 4,
                Opacity = InnerDisk.Opacity,
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center,
                Background = System.Windows.Media.Brushes.Transparent,
                BorderBrush = System.Windows.Media.Brushes.Transparent,
                Visibility = Visibility.Visible,
            };

            InnerDiskButton.Style = NewUnstyledButtonStyle();
            
            AutomationProperties.SetName(InnerDiskButton, "Dossier principal");
            
            InnerDiskButton.Click += InnerDisk_MouseLeftButtonDown;

            mainGrid.Children.Add(InnerDisk);
            mainGrid.Children.Add(InnerDiskButton);
        }

        private string GetDirectoryRelativePath(PieFolderItem dir)
        {
            if (dir.PrevFolder == null || dir.Name == null)
            {
                return "";
            }
            
            return GetDirectoryRelativePath(dir.PrevFolder) + dir.Name + "\\";
        }

        public string AutoRename(string refPath)
        {
            int i = 1;
            string newPath = refPath;
            string dirPath = Path.GetDirectoryName(newPath);
            string ext = Path.GetExtension(newPath);
            string name = string.Join(" ", Path.GetFileNameWithoutExtension(newPath).Split( Path.GetInvalidFileNameChars()));
            newPath = $"{dirPath}\\{name}{ext}";
            bool hasRenamed = false;
            while (File.Exists(newPath))
            {
                hasRenamed = true;
                newPath = $"{dirPath}\\{name} ({i}){ext}";
                i++;
            }

            if (hasRenamed) MessageBox.Show("Un raccourci avec le même nom est présent, le raccourci a été renommé " + Path.GetFileNameWithoutExtension(newPath));
            return newPath;
        }
        
        private string CreateUrlShortcut(string url, string shortcutPath)
        {
            shortcutPath += ".url";
            shortcutPath = AutoRename(shortcutPath);
            string shortcutContent = "[InternetShortcut]\r\n" +
                                     "URL=" + url + "\r\n";

            File.WriteAllText(shortcutPath, shortcutContent, Encoding.Unicode);
            return shortcutPath;
        }

        private string CreateShortcut(string targetPath, string shortcutPath)
        {
            string extension = Path.GetExtension(targetPath) == ".url" ? ".url" : ".lnk";
            shortcutPath += extension;
            shortcutPath = AutoRename(shortcutPath);
            if (extension == ".url")
            {
                File.Copy(targetPath, shortcutPath);
                return shortcutPath;
            }
            Win32Helper.CreateShortcut(targetPath, shortcutPath);
            return shortcutPath;
        }
        
        // Drag and drop capability, automagically add file/folder.
        // Used for both the inner and outer disks.
        private void Disk_Drop(object sender, DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                if (files != null && files.Length != 0)
                {
                    // TODO: This feels inefficient, but PieItemSettingsWindow(null, ...) feels like something might break
                    FileItemType type;

                    if (File.Exists(files[0]) || Directory.Exists(files[0]))
                    {
                        type = FileItemType.File;
                    }
                    else
                    {
                        return;
                    }
                
                    string relativePath = GetDirectoryRelativePath(CurrentFolder);
                    relativePath = Path.Join(Config.CustomDirLocation, relativePath);
                    relativePath = Path.Join(relativePath, Path.GetFileNameWithoutExtension(files[0]));
                    relativePath = CreateShortcut(files[0], relativePath);

                    PieItem newItem = new FileItem(true, Path.GetFileNameWithoutExtension(Path.GetFullPath(relativePath)), false, null, Path.GetFullPath(relativePath), null, type);
                    CurrentFolder.Items.Add(newItem);
                
                    RefreshFolder();
                    //Config.SaveItems(RootFolder);
                }
                else if (e.Data.GetDataPresent("text/x-moz-url"))    //Chrome, Firefox
                {
                    MemoryStream data = (MemoryStream)e.Data.GetData("text/x-moz-url");
                    if (data == null) return;
                    
                    string dataStr = Encoding.Unicode.GetString(data.ToArray());
                    string[] parts = dataStr.Split(((char)10));
                    string url = (string)e.Data.GetData(DataFormats.StringFormat);
                    if (string.IsNullOrEmpty(url)) return;
                    if (parts.Length > 0) url = parts[0];
                    string filename = new Uri(url).Host;
                    if (parts.Length > 1) filename = parts[1];
                    filename = filename.Replace("\0", "");
                    filename = filename.Trim();
                    
                    string relativePath = GetDirectoryRelativePath(CurrentFolder);
                    relativePath = Path.Join(Config.CustomDirLocation, relativePath);
                    
                    string shortcutPath = Path.Join(relativePath, filename);
                    shortcutPath = CreateUrlShortcut(url, shortcutPath);
                    PieItem newItem = new FileItem(true, Path.GetFileNameWithoutExtension(shortcutPath), false, null, Path.GetFullPath(shortcutPath), null, FileItemType.File);
                    CurrentFolder.Items.Add(newItem);
                    RefreshFolder();
                }
                else {
                    string url = (string)e.Data.GetData(DataFormats.StringFormat);
                    if (string.IsNullOrEmpty(url)) return;
                    
                    string host = new Uri(url).Host;
                    string relativePath = GetDirectoryRelativePath(CurrentFolder);
                    relativePath = Path.Join(Config.CustomDirLocation, relativePath);
                    
                    string shortcutPath = Path.Join(relativePath, host);
                    shortcutPath = CreateUrlShortcut(url, shortcutPath);
                    PieItem newItem = new FileItem(true, Path.GetFileNameWithoutExtension(shortcutPath), false, null, Path.GetFullPath(shortcutPath), null, FileItemType.File);
                    CurrentFolder.Items.Add(newItem);
                    RefreshFolder();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Une erreur est survenue: " + ex.Message);
            }
        }

        // Add items based on folder's items.
        // Also change center icon to folder's icon.
        public void SwitchToFolder(PieFolderItem folder)
        {
            ClearItems();

            CurrentFolder = folder;

            mainGrid.Children.Remove(InnerDisk);
            if (CurrentFolder != null && CurrentFolder != RootFolder)
            {
                AutomationProperties.SetName(InnerDiskButton, "Retour au dossier précédent");
                InnerDiskButton.Opacity = Config.InnerDiskNormalOpacity;
                InnerDiskButton.Content = InnerDisk;
            }
            else
            {
                AutomationProperties.SetName(InnerDiskButton, "Dossier principal");
                InnerDiskButton.Content = null;
                InnerDiskButton.Opacity = 0;
                mainGrid.Children.Add(InnerDisk);
            }
            
            // Create display info
            for (int i = 0; i < folder.Items.Count; i++)
            {
                CreateDisplayInfo(folder.Items[i], i, folder.Items.Count);
            }

            UpdateItemAppearance();

            // Create all buttons first and THEN labels, so labels are always on top.
            foreach(ItemDisplayInfo info in ItemDisplayInfos)
            {
                mainGrid.Children.Add(info.ItemButton);
            }

            foreach (ItemDisplayInfo info in ItemDisplayInfos)
            {
                mainGrid.Children.Add(info.ItemLabel);
            }

            // Change center icon
            if (Config.ChangingCenterIcon)
                InnerDisk.Source = folder.Icon.ImageBitmapSource;
        }

        // To be used if an item was changed in this folder.
        public void RefreshFolder()
        {
            SwitchToFolder(CurrentFolder);
        }

        // Refresh the "file not found" icons.
        // TODO: This is inefficient. Will refresh if not using a custom icon
        //       but successfully loaded an icon from the file.
        // TODO: This is also awful. Much better to be rid of the Copy() portion used somewhere
        //       and just directly pointed to Config.FileNotFoundIcon.
        public void ClearCachedImagesFileNotFound(PieFolderItem folder, string oldPath, string newPath)
        {
            foreach(PieItem item in folder.Items)
            {
                if(item is FileItem && !item.IsCustomIcon && item.Icon.ImagePath == oldPath)
                {
                    item.Icon.ClearCache();
                    item.Icon.ImagePath = newPath;
                }
                else if(item is PieFolderItem)
                {
                    ClearCachedImagesFileNotFound((PieFolderItem)item, oldPath, newPath);
                }
            }
        }

        // TODO: Similar here.
        public void ClearCachedImagesDefaultFolder(PieFolderItem folder, string oldPath, string newPath)
        {
            if (!folder.IsCustomIcon && folder.Icon.ImagePath == oldPath)
            {
                folder.Icon.ClearCache();
                folder.Icon.ImagePath = newPath;
            }                

            foreach(PieItem item in folder.Items)
            {
                if(item is PieFolderItem)
                {
                    ClearCachedImagesDefaultFolder((PieFolderItem)item, oldPath, newPath);
                }
            }
        }

        // This is used to update the variable appearance of the items.
        // Ex: name, icon, user-configurable sizes, etc...
        private void UpdateItemAppearance(ItemDisplayInfo itemInfo, int number, int totalItems, int iteration)
        {
            itemInfo.ItemButton.Width = Config.PieItemSize * Config.ScreenFactor;
            itemInfo.ItemButton.Height = Config.PieItemSize * Config.ScreenFactor;
            itemInfo.ItemButton.Opacity = Config.PieItemNormalOpacity;

            double scalingFactor = 1.0 + iteration * 0.6;
            // To be spread evenly in a circle.
            var buttonMargin = new Thickness();
            buttonMargin.Left = Config.PieItemRadiusFromCenter * scalingFactor * Config.ScreenFactor * Math.Cos(number * 2 * Math.PI / totalItems);
            buttonMargin.Top = Config.PieItemRadiusFromCenter * scalingFactor * Config.ScreenFactor * Math.Sin(number * 2 * Math.PI / totalItems);
            itemInfo.ItemButton.Margin = buttonMargin;

            // This was initialized as an Image() during CreateDisplayInfo().
            ((Image)itemInfo.ItemButton.Content).Source = itemInfo.Item.Icon.ImageBitmapSource;

            var labelMargin = new Thickness();
            labelMargin.Left = itemInfo.ItemButton.Margin.Left;
            labelMargin.Top = itemInfo.ItemButton.Margin.Top + Config.PieItemSize * Config.ScreenFactor + Config.PieItemLabelPadding; // TODO: Figure out proper padding.
            itemInfo.ItemLabel.Margin = labelMargin;

            itemInfo.ItemLabel.FontSize = Config.FontSize;
            itemInfo.ItemLabel.Foreground = new SolidColorBrush(Config.PieItemLabelColor);

            itemInfo.ItemLabel.Content = (itemInfo.Item.Name == null) ? "" : itemInfo.Item.Name;
        }

        public void UpdateItemAppearance()
        {
            int itemsCount = ItemDisplayInfos.Count;
            int groupSize = 16;
            int iterations = (itemsCount + groupSize - 1) / groupSize; // Calculate the number of iterations

            //for (int iteration = 0; iteration < iterations; iteration++)
            //{
            //    int startIndex = iteration * groupSize;
            //    int endIndex = Math.Min(startIndex + groupSize - 1, itemsCount - 1);
            //    int totalCount = endIndex - startIndex + 1;

            //    for (int it = 0; it < totalCount; it++)
            //    {
            //        int itemIndex = startIndex + it;
            //        UpdateItemAppearance(ItemDisplayInfos[itemIndex], it, totalCount, iteration);
            //    }
            //}
            
            for (int i = 0; i < ItemDisplayInfos.Count; i++)
            {
                UpdateItemAppearance(ItemDisplayInfos[i], i, ItemDisplayInfos.Count, 0);
            }
        }

        private Style NewUnstyledButtonStyle()
        {
            // Create a new style for the button
            Style buttonStyle = new Style(typeof(Button));

            // Control template is what manages controls (buttons etc...)
            // We create one based on buttons
            ControlTemplate template = new ControlTemplate(typeof(Button));

            // Create the root element of the control template
            // This is a border
            FrameworkElementFactory root = new FrameworkElementFactory(typeof(Border));

            // Set background, border, and content properties for the root element
            root.SetValue(Border.BackgroundProperty, new SolidColorBrush(Colors.Transparent));
            root.SetValue(Border.BorderBrushProperty, new SolidColorBrush(Colors.Transparent));
            root.SetValue(Border.BorderThicknessProperty, new Thickness(0));

            // Create the content presenter, the ContentPresenter goal is to render it's child
            FrameworkElementFactory contentPresenter = new FrameworkElementFactory(typeof(ContentPresenter));
            
            // Add the content presenter to the root element
            root.AppendChild(contentPresenter);

            // Set the visual tree of the control template
            template.VisualTree = root;

            // Set the control template for the button style
            buttonStyle.Setters.Add(new Setter(TemplateProperty, template));

            return buttonStyle;
        }
        
        private void ClearItems()
        {
            foreach (ItemDisplayInfo info in ItemDisplayInfos)
            {
                mainGrid.Children.Remove(info.ItemButton);
                mainGrid.Children.Remove(info.ItemLabel);
            }

            ItemDisplayInfos.Clear();
        }

        // This is a bit more complicated.
        // We have to account for all the stuff that hovering over/pressing/etc... on an item does.
        private void CreateDisplayInfo(PieItem pieItem, int number, int totalItems)
        {
            ItemDisplayInfo itemInfo = new ItemDisplayInfo();

            // The PieItem
            itemInfo.Item = pieItem;


            // The Button control to be used in the main window.
            // Here, we set the non-user-configurable options.
            itemInfo.ItemButton = new Button();

            itemInfo.ItemButton.HorizontalAlignment = HorizontalAlignment.Stretch;
            itemInfo.ItemButton.VerticalAlignment = VerticalAlignment.Stretch;
            itemInfo.ItemButton.HorizontalContentAlignment = HorizontalAlignment.Stretch;
            itemInfo.ItemButton.VerticalContentAlignment = VerticalAlignment.Stretch;

            // Apply the custom style to the button
            itemInfo.ItemButton.Style = NewUnstyledButtonStyle();
            
            itemInfo.ItemButton.Background = System.Windows.Media.Brushes.Transparent;
            itemInfo.ItemButton.BorderBrush = System.Windows.Media.Brushes.Transparent;

            // Currently just a blank image, will actually be set to something later on.
            var itemImage = new Image();
            itemImage.Stretch = Stretch.Fill;
            itemImage.StretchDirection = StretchDirection.Both;
            itemInfo.ItemButton.Content = itemImage;
            AutomationProperties.SetName(itemInfo.ItemButton, itemInfo.Item.Name);
            
            // The Label control to be used in the main window.
            itemInfo.ItemLabel = new Label();

            itemInfo.ItemLabel.HorizontalAlignment = HorizontalAlignment.Center;
            itemInfo.ItemLabel.VerticalAlignment = VerticalAlignment.Center;

            // This fixes the flashing button issue if the mouse is on top of both the button and the label.
            itemInfo.ItemLabel.IsHitTestVisible = false;

            // Invisible by default. Will activate only on mouse hover on button.
            itemInfo.ItemLabel.Visibility = Visibility.Visible;
            itemInfo.ItemLabel.Foreground = new SolidColorBrush(Colors.White);
            itemInfo.ItemLabel.FontSize = 1;
            
            UpdateItemAppearance(itemInfo, number, totalItems, 0);


            itemInfo.ItemButton.MouseEnter +=
                (s, e) =>
                {
                    // itemInfo.ItemLabel.Visibility = Visibility.Visible;
                };

            itemInfo.ItemButton.MouseLeave +=
                (s, e) =>
                {
                    //itemInfo.ItemLabel.Visibility = Visibility.Hidden;
                };
            
            itemInfo.ItemButton.GotKeyboardFocus +=
                (s, e) =>
                {
                   // itemInfo.ItemLabel.Visibility = Visibility.Visible;
                };
            
            itemInfo.ItemButton.LostKeyboardFocus +=
                (s, e) =>
                {
                   // itemInfo.ItemLabel.Visibility = Visibility.Hidden;
                };
            
            itemInfo.ItemButton.Click +=
                (s, e) =>
                {
                    itemInfo.Item.LeftClick(this);
                };

            // Create the context menu
            
            /*
                MenuItem settingsMenuItem = new MenuItem();
                settingsMenuItem.Header = "Item settings";
                settingsMenuItem.Click +=
                (s, e) =>
                {
                    PieItemSettingsWindow pieItemSettingsWindow = new PieItemSettingsWindow(pieItem, CurrentFolder);
                    pieItemSettingsWindow.ShowDialog();

                    // Replace the old pieItem with the new one
                    if (pieItemSettingsWindow.Saved)
                    {
                        int i = CurrentFolder.Items.IndexOf(pieItem);

                        // You know, I feel like this should never happen
                        if (i == -1)
                            return;

                        CurrentFolder.Items[i] = pieItemSettingsWindow.NewPieItem;
                        RefreshFolder();
                        Config.SaveItems(RootFolder);
                    }
                };
             */
            
            MenuItem removeMenuItem = new MenuItem();
            removeMenuItem.Header = "Supprimer";
            removeMenuItem.Click +=
                (s, e) =>
                {
                    if (MessageBox.Show("Êtes vous sûr de vouloir supprimer ce raccourci ?", "Supprimer raccourci", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        return;
                    if (pieItem.FilePath == "")
                        return;
                    try
                    {
                        if(File.Exists(pieItem.FilePath))
                        {
                            File.Delete(pieItem.FilePath);
                        }
                        else if(Directory.Exists(pieItem.FilePath))
                        {
                            Directory.Delete(pieItem.FilePath);
                        }
                        else return;
                        CurrentFolder.Items.Remove(pieItem);
                        RefreshFolder();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Impossible de supprimer le raccourci: " + ex);
                    }
                };


            MenuItem renameMenuItem = new MenuItem();
            renameMenuItem.Header = "Renommer";
            renameMenuItem.Click += (s, e) =>
            {
                
                string refPath = pieItem.FilePath;
                string name = Path.GetFileNameWithoutExtension(refPath);
                RenameDialog inputDialog = new RenameDialog("Entrez un nom de fichier:", name);
                bool? result = inputDialog.ShowDialog();
                
                if (result == true && inputDialog.UserInput != name)
                {
                    string dirPath = Path.GetDirectoryName(refPath);
                    string ext = Path.GetExtension(refPath);
                    try
                    {
                        string newPath = AutoRename($"{dirPath}\\{inputDialog.UserInput}{ext}");
                        File.Move(refPath, newPath);
                        pieItem.Name =  Path.GetFileNameWithoutExtension(newPath);
                        pieItem.FilePath = newPath;
                        SwitchToFolder(CurrentFolder);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            };
            
            if (!itemInfo.Item.IsProtected)
            {            
                itemInfo.ItemButton.ContextMenu = new ContextMenu();
                //itemInfo.ItemButton.ContextMenu.Items.Add(settingsMenuItem);
                itemInfo.ItemButton.ContextMenu.Items.Add(removeMenuItem);
                itemInfo.ItemButton.ContextMenu.Items.Add(renameMenuItem);
            }


            //TODO: Figure out transparency of background/border on hover.

            ItemDisplayInfos.Add(itemInfo);
        }

        private void InnerDisk_MouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            if (CurrentFolder != RootFolder)
            {
                SwitchToFolder(CurrentFolder.PrevFolder);
            }
        }
        
        private void InnerDisk_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (CurrentFolder != RootFolder)
            {
                SwitchToFolder(CurrentFolder.PrevFolder);
            }
        }

        private void OuterDisk_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // TODO: There is a lot of code duplication between the inner disk and outer disk methods. Fix.
            InnerDisk.Opacity = Config.InnerDiskMouseDownOpacity;
            OuterDisk.Opacity = Config.OuterDiskMouseDownOpacity;

            foreach (ItemDisplayInfo info in ItemDisplayInfos)
            {
                info.ItemButton.Opacity = Config.PieItemMouseDownOpacity;
            }

            //DragMove();

            InnerDisk.Opacity = Config.InnerDiskNormalOpacity;
            OuterDisk.Opacity = Config.OuterDiskNormalOpacity;

            foreach (ItemDisplayInfo info in ItemDisplayInfos)
            {
                info.ItemButton.Opacity = Config.PieItemNormalOpacity;
            }
        }

        private struct ItemDisplayInfo
        {
            public PieItem Item { get; set; }
            public Button ItemButton { get; set; }
            public Label ItemLabel { get; set; }
            public TextBlock ItemText { get; set; }
            public bool MouseDown { get; set; } // If mouse holding onto it, but not necessarily dragging
            public bool Dragging { get; set; } // If the item has explicitly started moving

            public System.Windows.Point InitialMousePosition { get; set; }
        }

        private void MainWindow_OnLocationChanged(object sender, EventArgs e)
        {
            ResetPosition();
        }
    }
}
