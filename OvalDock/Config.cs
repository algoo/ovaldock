﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Xml;
using MessageBox = System.Windows.Forms.MessageBox;
using System.Windows.Media;

namespace OvalDock
{
    class Config
    {
        public static string ProgramName { get; private set; }

        // TODO: Migrate InnerDiskImagePath and OuterDiskImagePath to CachedImage ?
        
        public static double WindowPositionPercentageWidth { get; set; }
        public static double FontSize { get; set; }
        
        public static double WindowPositionPercentageHeight { get; set; }
        
        private static string _InnerDiskImagePath { get; set; }
        public static string InnerDiskImagePath => Environment.ExpandEnvironmentVariables(_InnerDiskImagePath);

        public static double InnerRadius { get; set; }
        public static double InnerDiskNormalOpacity { get; set; }
        public static double InnerDiskMouseDownOpacity { get; set; }

        private static string _OuterDiskImagePath { get; set; }
        public static string OuterDiskImagePath => Environment.ExpandEnvironmentVariables(_OuterDiskImagePath);

        public static double OuterRadius { get; set; }
        public static double OuterDiskNormalOpacity { get; set; }
        public static double OuterDiskMouseDownOpacity { get; set; }

        public static CachedImage FileNotFoundIcon { get; private set; }
        public static CachedImage FolderDefaultIcon { get; private set; }

        public static double PieItemSize { get; set; }
        public static double PieItemNormalOpacity { get; set; }
        public static double PieItemMouseDownOpacity { get; set; }
        public static double PieItemRadiusFromCenter { get; set; }

        public static int PieItemLabelPadding { get; private set; }

        public static int PieItemLabelSize { get; private set; }

        public static Color PieItemLabelColor { get; set; }

        private static string _ProtectedDirLocation { get; set; }
        public static string ProtectedDirLocation => Environment.ExpandEnvironmentVariables(_ProtectedDirLocation);

        private static string _CustomDirLocation { get; set; }
        public static string CustomDirLocation => Environment.ExpandEnvironmentVariables(_CustomDirLocation);

        public static string UserConsent { get; set; }
        
               

        private static string ItemSaveLocation { get; set; }

        private static string ProgramSaveLocation { get; set; }
        private static string CustomSaveLocation { get; set; }
        private static string UserDir { get; set; }

        public static uint HotkeyModifiers { get; set; }
        public static uint Hotkey { get; private set; }

        public static double PieItemDragSensitivity { get; private set; }
        
        public static double ScreenFactor { get; set; }
        
        public static bool ChangingCenterIcon { get; set; }

        public static bool AltHotkey { get; set; }

        public static bool DirectoryHasWriteAccess(string path)
        {
            try
            {
                var f = File.Create(path + "\\.test");
                f.Close();
                File.Delete(path + "\\.test");
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (DirectoryNotFoundException)
            {
                return false;
            }
        }
        
        public static bool FileHasWriteAccess(string path)
        {
            try
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(path);
                return directoryInfo.Exists &&
                       (directoryInfo.Attributes & FileAttributes.ReadOnly) != FileAttributes.ReadOnly;
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (DirectoryNotFoundException)
            {
                return false;
            }
        }
        
        public static void LoadDefaultProgramSettings()
        {
            ProgramName = "OvalDock";

            ItemSaveLocation = @".\config\items.xml";
            ProgramSaveLocation = @".\config\config.xml";

            UserDir = Environment.ExpandEnvironmentVariables(@"%APPDATA%\OvalDock");
            CustomSaveLocation = Path.Join(UserDir, @"config.xml");
            _CustomDirLocation = Path.Join(UserDir, @"custom");

            _InnerDiskImagePath = @".\icons\inner.png";
            InnerRadius = 50;
            InnerDiskNormalOpacity = 1.0;
            InnerDiskMouseDownOpacity = 0.5;

            _OuterDiskImagePath = @".\icons\circle.png";
            OuterRadius = 160;
            OuterDiskNormalOpacity = 1.0;
            OuterDiskMouseDownOpacity = 0.5;

            FileNotFoundIcon = new CachedImage();
            FileNotFoundIcon.ImagePath = @".\icons\file_not_found.png";
            FolderDefaultIcon = new CachedImage();
            FolderDefaultIcon.ImagePath = @".\icons\folder.png";
            PieItemSize = 60;
            PieItemNormalOpacity = 1.0;
            PieItemMouseDownOpacity = 0.5;
            PieItemRadiusFromCenter = 190;

            PieItemLabelPadding = 20;
            PieItemLabelSize = 20;
            PieItemLabelColor = Color.FromRgb(255, 255, 255);

            PieItemDragSensitivity = 20;
            _ProtectedDirLocation = @".\config\protected";

            WindowPositionPercentageWidth = 50;
            WindowPositionPercentageHeight = 50;

            FontSize = PieItemLabelSize;
            UserConsent = "";

            ChangingCenterIcon = true;
            AltHotkey = false;

            // TODO: SO APPARENTLY, MOD_WIN NEEDS TO BE A MODIFIER IF YOU WANT TO USE IT AS theHotKey AS WELL
            //       Double check!
            HotkeyModifiers = Win32Helper.MOD_CONTROL + Win32Helper.MOD_WIN + Win32Helper.MOD_SHIFT;
            Hotkey = Win32Helper.VK_LWIN;

            try
            {
                if (!Directory.Exists(UserDir))
                {
                    Directory.CreateDirectory(UserDir);
                }
            }
            catch (System.Exception e)
            {
                MessageBox.Show("Impossible de créer le dossier utilisateur" + e.Message);
                throw;
            }
            
            ComputeScreenFactor();
        }

        public static void ComputeScreenFactor()
        {
            ScreenFactor = SystemParameters.PrimaryScreenWidth > SystemParameters.PrimaryScreenHeight
                ? SystemParameters.PrimaryScreenWidth
                : SystemParameters.PrimaryScreenHeight;
            ScreenFactor /= 100;
        }
        
        public static void SaveItems(PieFolderItem root)
        {
            XmlDocument config = new XmlDocument();
            XmlElement rootNode = config.CreateElement("Root");
            config.AppendChild(rootNode);

            root.SaveConfig(config.DocumentElement);

            config.Save(ItemSaveLocation);
        }

        public static PieFolderItem LoadItems()
        {
            XmlDocument config = new XmlDocument();
            PieFolderItem rootFolder = new PieFolderItem(false, null, true, InnerDiskImagePath, null, "");

            try
            {
                config.Load(ItemSaveLocation);
                rootFolder.LoadConfig(config.DocumentElement);
            }
            catch (Exception e) // TODO: You know, I should really handle this stuff better.
            {
                Console.WriteLine(e);
            }

            return rootFolder;
        }

        private static Dictionary<string, string> InitSettingsDictionary()
        {
            var ret = new Dictionary<string, string>();
            
            ret["InnerDiskImagePath"]           = "";
            ret["InnerRadius"]                  = "";
            ret["InnerDiskNormalOpacity"]       = "";
            ret["InnerDiskMouseDownOpacity"]    = "";
            ret["OuterDiskImagePath"]           = "";
            ret["OuterRadius"]                  = "";
            ret["OuterDiskNormalOpacity"]       = "";
            ret["OuterDiskMouseDownOpacity"]    = "";
            ret["FileNotFoundIcon"]             = "";
            ret["FolderDefaultIcon"]            = "";
            ret["PieItemSize"]                  = "";
            ret["PieItemNormalOpacity"]         = "";
            ret["PieItemMouseDownOpacity"]      = "";
            ret["PieItemRadiusFromCenter"]      = "";
            ret["PieItemLabelColor"]            = "";
            ret["ProtectedDirLocation"]         = "";
            ret["CustomDirLocation"]            = "";
            ret["UserConsent"]                  = "";
            ret["WindowPositionPercentageWidth"] = "";
            ret["WindowPositionPercentageHeight"] = "";
            ret["ChangingCenterIcon"] = "";
            ret["AltHotkey"] = "";
            ret["FontSize"] = "";

            return ret;
        }
        
        private static Dictionary<string, string> ParseSettingsFileToDictionary(Dictionary<string, string> dict, XmlElement rootNode)
        {
            foreach (XmlElement node in rootNode.ChildNodes)
            {
                dict[node.Name] = node.InnerText;
            }

            return dict;
        }

        private static void MapSettingsDictionaryToSettings(Dictionary<string, string> dict)
        {
            _InnerDiskImagePath          = dict["InnerDiskImagePath"]        == "" ? _InnerDiskImagePath          : dict["InnerDiskImagePath"];
            InnerRadius                 = dict["InnerRadius"]               == "" ? InnerRadius                 : double.Parse(dict["InnerRadius"]);
            InnerDiskNormalOpacity      = dict["InnerDiskNormalOpacity"]    == "" ? InnerDiskNormalOpacity      : double.Parse(dict["InnerDiskNormalOpacity"]);
            InnerDiskMouseDownOpacity   = dict["InnerDiskMouseDownOpacity"] == "" ? InnerDiskMouseDownOpacity   : double.Parse(dict["InnerDiskMouseDownOpacity"]);
            
            _OuterDiskImagePath          = dict["OuterDiskImagePath"]        == "" ? _OuterDiskImagePath          : dict["OuterDiskImagePath"];
            OuterRadius                 = dict["OuterRadius"]               == "" ? OuterRadius                 : double.Parse(dict["OuterRadius"]);
            OuterDiskNormalOpacity      = dict["OuterDiskNormalOpacity"]    == "" ? OuterDiskNormalOpacity      : double.Parse(dict["OuterDiskNormalOpacity"]);
            OuterDiskMouseDownOpacity   = dict["OuterDiskMouseDownOpacity"] == "" ? OuterDiskMouseDownOpacity   : double.Parse(dict["OuterDiskMouseDownOpacity"]);
            
            FileNotFoundIcon.ImagePath  = dict["FileNotFoundIcon"]          == "" ? FileNotFoundIcon.ImagePath  : dict["FileNotFoundIcon"];
            FolderDefaultIcon.ImagePath = dict["FolderDefaultIcon"]         == "" ? FolderDefaultIcon.ImagePath : dict["FolderDefaultIcon"];
            
            PieItemSize                 = dict["PieItemSize"]               == "" ? PieItemSize                 : double.Parse(dict["PieItemSize"]);
            PieItemNormalOpacity        = dict["PieItemNormalOpacity"]      == "" ? PieItemNormalOpacity        : double.Parse(dict["PieItemNormalOpacity"]);
            PieItemMouseDownOpacity     = dict["PieItemMouseDownOpacity"]   == "" ? PieItemMouseDownOpacity     : double.Parse(dict["PieItemMouseDownOpacity"]);
            PieItemRadiusFromCenter     = dict["PieItemRadiusFromCenter"]   == "" ? PieItemRadiusFromCenter     : double.Parse(dict["PieItemRadiusFromCenter"]);

            try
            {
                PieItemLabelColor = dict["PieItemLabelColor"] == "" ? PieItemLabelColor : (Color)ColorConverter.ConvertFromString(dict["PieItemLabelColor"]);
            } catch
            {
                MessageBox.Show("Une erreur est survenue lors de la lecture de la couleur dans le fichier de configuration. Assurez vous que celui-ci ne comporte pas d'erreur\n", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                PieItemLabelColor = PieItemLabelColor;
            }

            _ProtectedDirLocation        = dict["ProtectedDirLocation"]      == "" ? _ProtectedDirLocation        : dict["ProtectedDirLocation"];
            _CustomDirLocation           = dict["CustomDirLocation"]         == "" ? _CustomDirLocation           : dict["CustomDirLocation"];
            
            UserConsent                 = dict["UserConsent"]               == "" ? UserConsent                 : dict["UserConsent"].ToLower();
            
            WindowPositionPercentageWidth   = dict["WindowPositionPercentageWidth"] == "" ? WindowPositionPercentageWidth   :   double.Parse(dict["WindowPositionPercentageWidth"]);
            WindowPositionPercentageHeight  = dict["WindowPositionPercentageHeight"] == "" ? WindowPositionPercentageHeight :   double.Parse(dict["WindowPositionPercentageHeight"]);

            AltHotkey = dict["AltHotkey"] == "" ? AltHotkey : dict["AltHotkey"].ToLower() == "true";
            ChangingCenterIcon = dict["ChangingCenterIcon"] == "" ? ChangingCenterIcon : dict["ChangingCenterIcon"].ToLower() == "true";
            
            FontSize        = dict["FontSize"]      == "" ? FontSize        : double.Parse(dict["FontSize"]);

            FileNotFoundIcon.ClearCache();
            FolderDefaultIcon.ClearCache();
        }
        
                // Hopefully no typos in here.
        public static void SaveProgramSettings()
        {
            XmlDocument config = new XmlDocument();
            XmlElement rootNode = config.CreateElement("Root");
            config.AppendChild(rootNode);

            // TODO: ML - Ajouter une config pour la position du dock sur l'écran, pourcentage ?
            // TODO: There is a lot of code repetition in rounding doubles. Fix.

            var dict = InitSettingsDictionary();
            
            dict["InnerDiskImagePath"]          = _InnerDiskImagePath ?? "";
            dict["InnerRadius"]                 = InnerRadius.ToString("0");
            dict["InnerDiskNormalOpacity"]      = InnerDiskNormalOpacity.ToString("0.##");
            dict["InnerDiskMouseDownOpacity"]   = InnerDiskMouseDownOpacity.ToString("0.##");
            dict["OuterDiskImagePath"]          = _OuterDiskImagePath ?? "";
            dict["OuterRadius"]                 = OuterRadius.ToString("0");
            dict["OuterDiskNormalOpacity"]      = OuterDiskNormalOpacity.ToString("0.##");
            dict["OuterDiskMouseDownOpacity"]   = OuterDiskMouseDownOpacity.ToString("0.##");
            dict["FileNotFoundIcon"]            = FileNotFoundIcon.ImagePath ?? "";
            dict["FolderDefaultIcon"]           = FolderDefaultIcon.ImagePath ?? "";
            dict["PieItemSize"]                 = PieItemSize.ToString("0");
            dict["PieItemNormalOpacity"]        = PieItemNormalOpacity.ToString("0.##");
            dict["PieItemMouseDownOpacity"]     = PieItemMouseDownOpacity.ToString("0.##");
            dict["PieItemRadiusFromCenter"]     = PieItemRadiusFromCenter.ToString("0");
            dict["PieItemLabelColor"]           = PieItemLabelColor.ToString();
            dict["ProtectedDirLocation"]        = _ProtectedDirLocation ?? "";
            dict["CustomDirLocation"]           = _CustomDirLocation ?? "";
            dict["UserConsent"]                 = UserConsent ?? "";
            dict["WindowPositionPercentageWidth"] = WindowPositionPercentageWidth.ToString("0.##");
            dict["WindowPositionPercentageHeight"] = WindowPositionPercentageHeight.ToString("0.##");
            dict["AltHotkey"] = AltHotkey ? "true" : "false";
            dict["ChangingCenterIcon"] = ChangingCenterIcon ? "true" : "false";
            dict["FontSize"] = FontSize.ToString("0.##");

            try
            {
                config.Save(CustomSaveLocation);
                foreach (var element in dict)
                {
                    XmlElement node = config.CreateElement(element.Key);
                    node.InnerText = element.Value;
                    rootNode.AppendChild(node);
                }
                config.Save(CustomSaveLocation);
            }
            catch (Exception)
            {
                MessageBox.Show("OvalDock n'a pas les permissions d'écriture sur le fichier de configuration, cela peut engendrer des défauts de fonctionnement");
            }
        }
        
        // I really hope there's no typos in here either.
        public static void LoadProgramSettings()
        {
            // Settings will get overwritten on a per setting basis.
            // Any setting not overwritten will hence be the default.
            LoadDefaultProgramSettings();

            XmlDocument config = new XmlDocument();

            try
            {
                if (!File.Exists(CustomSaveLocation))
                {
                    File.Copy(ProgramSaveLocation, CustomSaveLocation);
                }
                config.Load(CustomSaveLocation);
            }
            catch(Exception e)
            {
                MessageBox.Show("Impossible de lire la configuration: " + e.Message);
                return;
            }

            XmlElement rootNode = config.DocumentElement;

            // NOTE: ML - Ici pour lire des configuration supplémentaires
            // TODO: ML - Ajouter une config pour la position du dock sur l'écran, pourcentage ?
            // FIX: ML - La lecture / écriture des floats / doubles est capricieuse, il vaut mieux utiliser un int allant de 0 a 100(0)
            // TODO: You know, I feel like some of this could be just a little automated.
            //       Probably keep an array of a custom Property class?

            if (rootNode == null)
                return;
            string[] args = Environment.GetCommandLineArgs();
            var settingsDict = InitSettingsDictionary();
            settingsDict = ParseSettingsFileToDictionary(settingsDict, rootNode);

            try
            {
                MapSettingsDictionaryToSettings(settingsDict);
                
                if (args.Length >= 2 && args[1] == "--check-startup-config")
                {
                    if (UserConsent == "")
                    {
                        UserConsent = "false";
                        var result = MessageBox.Show("Souhaitez-vous installer la nouvelle interface graphique circulaire, pour accéder à vos logiciels ? (choix réversible)", "Lancer au démarrage", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            UserConsent = "true";
                        }
                        SaveProgramSettings();
                    }
                }
                
            }
            catch (Exception e)
            {
                MessageBox.Show("Une erreur est survenue lors de la lecture du fichier de configuration. Assurez vous que celui-ci ne comporte pas d'erreur\n" + e.Message, "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
