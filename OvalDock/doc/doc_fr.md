﻿# _Documentation utilisateur d'OvalDock_

_OvalDock_ est un logiciel de dock conçu pour Windows 7 à 11. Il est positionné sur l'écran principal et fournit des 
raccourcis et des dossiers virtuels de raccourcis. 
Cette documentation utilisateur vous guidera à travers les fonctionnalités et l'utilisation d'_OvalDock_.

## Installation

Pour installer _OvalDock_ :

1. Extraire les fichiers _OvalDock_ vers un dossier de votre choix sur votre ordinateur.
2. Lancer le fichier `ovaldock.exe` à partir du dossier extrait.

Il peut être requis d'installer les redistribuables de l'architecture appropriée avant de lancer _OvalDock_
les installeurs des redistribuables sont trouvables dans le dossier `./redistribuables`.

## Utilisation

### Interface du dock

L'interface d'_OvalDock_ est composée de raccourcis et de dossiers virtuels.

- Cliquer sur un raccourcis pour le lancer.
- Pour naviguer à travers un dossier virtuel, cliquer dessus.
- Pour revenir en arrière, cliquer sur l'image centrale du dock.

### Analyse des dossiers

Au démarrage, _OvalDock_ analyse deux dossiers : le dossier protégé et le dossier personnalisé, qui sont spécifiés dans le fichier de configuration. (voir plus bas)
Le processus d'analyse commence par le dossier protégé, en copiant sa structure de dossiers sans les fichiers vers le dossier personnalisé.
Ce processus n'écrase pas le contenu du dossier custom dans le cas ou un dossier existerait déjà

Les fichiers et dossiers cachés sont ignorés lors de l'analyse.

Si un fichier portant le même nom et la même position qu'un fichier protégé existe dans le dossier personnalisé, celui-ci est ignoré.

### Ajout de raccourcis

Il est possible d'ajouter des raccourcis à _OvalDock_ en faisant glisser-déposer des fichiers sur le dock. Le raccourci va être ajouté dans le dosser actuel
Cette action crée un nouveau raccourci dans le dossier personnalisé qui pointe vers le fichier déposé.

### Menu contextuel

Un clic droit sur le dock ou sur un raccourci dans le dossier personnalisé ouvre un menu contextuel avec diverses options :

#### Menu contextuel du dock

- Rafraîchir : Analyse à nouveau les dossiers protégés et personnalisés pour afficher les modifications manuelles.
- Paramètres du dock : Ouvre un menu de paramètres visuels pour _OvalDock_.
- Masquer : Masque le dock de la vue.
- Quitter : Ferme _OvalDock_.
- Réinitialiser : Efface le contenu du dossier personnalisé.

#### Menu contextuel du raccourci

Un clic droit sur un raccourci dans le dossier personnalisé ouvre un menu contextuel avec deux options : Supprimer et Renommer.
Sélectionner l'option Supprimer envoie le raccourci dans la corbeille.

### Menu contextuel de l'icône de la barre d'état

_OvalDock_ propose également une icône dans la barre d'état système. Un clic droit sur l'icône de la barre d'état ouvre un menu contextuel avec les options suivantes :

- Paramètres du dock : Ouvre un menu de paramètres visuels pour _OvalDock_.
- Masquer / Afficher : Bascule la visibilité du dock.
- Quitter : Ferme _OvalDock_.

### Paramètres du dock

_OvalDock_ propose une fenêtre de paramètres où vous pouvez personnaliser différents aspects du dock. Les options disponibles comprennent :

- Rayon de l'icône centrale
- Opacité de l'icône centrale
- Rayon du disque
- Opacité du disque
- Taille des icônes
- Opacité des icônes
- Distance des icônes par rapport au centre
- Taille et couleur de la police
- Consentement de l'utilisateur

### Raccourcis clavier

Pour ramener _OvalDock_ au premier plan ou le réafficher, utiliser le raccourci clavier `Ctrl + Windows + Shift`.

## Options de lancement

_OvalDock_ accepte une option de lancement `--check-startup-config` avec le comportement suivant :

- Si le champ `UserConsent` dans le fichier de configuration est défini sur `false`, _OvalDock_ se ferme immédiatement.
- Si le champ `UserConsent` est défini sur `true`, _OvalDock_ démarre normalement.
- Si le champ `UserConsent` n'est pas présent, _OvalDock_ demande le consentement de l'utilisateur et enregistre la réponse dans le champ `UserConsent`.
- Pour demander à nouveau le consentement, supprimer le champ UserConsent du fichier de configuration.
- L'omission de cette option de lancement ignorera le consentement et démarrera _OvalDock_ normalement.

## Accessibilité

_OvalDock_ est conçu en tenant compte de l'accessibilité pour garantir une expérience utilisateur fluide aux personnes ayant des besoins différents. Voici les fonctionnalités d'accessibilité disponibles dans _OvalDock_ :

### Navigation au clavier

_OvalDock_ prend en charge l'utilisation complète du clavier, permettant aux utilisateurs de naviguer dans l'interface à l'aide de la touche Tab.
- Utilisation de la touche Tab pour déplacer le focus entre les raccourcis et les dossiers virtuels.
- Appuyer sur la touche Entrée ou Espace active l'élément sélectionné (navigation / lancement des raccourcis).

### Lecteurs d'écran

_OvalDock_ est compatible avec les logiciels de lecteur d'écran, ce qui le rend accessible aux personnes qui dépendent des commentaires audio. Les lecteurs d'écran suivants sont pris en charge :

- Narrateur Windows : _OvalDock_ est entièrement compatible avec Narrateur Windows, permettant aux utilisateurs de naviguer et d'interagir avec le dock à l'aide du lecteur d'écran.
- Narrateur NVDA : _OvalDock_ prend en charge Narrateur NVDA, assurant ainsi l'accessibilité aux utilisateurs qui préfèrent ce lecteur d'écran.

## Limitations

- _OvalDock_ ne peut pas lancer directement les fichiers `.exe`, mais peut lancer des raccourcis de fichiers `.exe`.
- Si la taille ou l'emplacement d'_OvalDock_ est incorrect, souvent après un changement d'écran principal,
  appuyer sur Ctrl + Win réajustera sa position et sa taille.
- Les performances et l'aspect visuel d'_OvalDock_ dépendent des capacités de l'ordinateur.
  À mesure que le nombre de raccourcis dans un dossier virtuel augmente, ils peuvent être positionnés de manière rapprochée,
  rendant plus difficile leur différenciation.

## Documentation du fichier de configuration

Le fichier de configuration d'_OvalDock_ contient différents paramètres qui contrôlent l'apparence et le comportement du dock. 
Cette documentation vous guidera à travers les différents éléments du fichier de configuration et leur objectif respectif.

Le fichier de configuration suit une structure XML avec un élément racine `<Root>`. 
À l'intérieur de l'élément racine, vous trouverez plusieurs éléments enfants représentant différents paramètres de configuration.

Au démarrage, __OvalDock__ va vérifier l'existence du fichier `%appdata%/OvalDock/config.xml`, si il n'existe pas, il va copier
`./config/config.xml` vers `%appdata%/OvalDock/config.xml`, ce fichier servira de fichier de configuration par utilisateur.

### Emplacement du dock

- `<WindowPositionPercentageWidth>`: Définit la position du centre du dock en pourcentage sur l'axe X de l'écran principal
- `<WindowPositionPercentageHeight>`: Définit la position du centre du dock en pourcentage sur l'axe Y de l'écran principal

### Configuration de l'icône centrale

- `<InnerDiskImagePath>` : Spécifie le chemin du fichier de l'image de l'icône centrale. L'image sera affichée au centre du dock.
- `<InnerRadius>` : Définit le rayon de l'icône centrale.
- `<InnerDiskNormalOpacity>` : Définit le niveau d'opacité de l'icône centrale. (0,0 - 1,0)

### Configuration du disque

- `<OuterDiskImagePath>` : Spécifie le chemin du fichier de l'image du disque.
- `<OuterRadius>` : Définit le rayon du disque.
- `<OuterDiskNormalOpacity>` : Définit le niveau d'opacité du disque. (0,0 - 1,0)

### Configuration des icônes

- `<FileNotFoundIcon>` : Spécifie le chemin du fichier de l'icône affichée lorsque le fichier n'est pas trouvé ou inaccessible.
- `<FolderDefaultIcon>` : Définit l'icône par défaut des dossiers dans le dock.

### Configuration des éléments

- `<PieItemSize>` : Spécifie la taille de chaque élément (raccourci) affiché dans le dock.
- `<PieItemNormalOpacity>` : Définit le niveau d'opacité des éléments (raccourcis). (0,0 - 1,0)
- `<PieItemRadiusFromCenter>` : Définit la distance des éléments (raccourcis) par rapport au centre du dock.

### Emplacements des dossiers

- `<ProtectedDirLocation>` : Spécifie l'emplacement du dossier protégé. _OvalDock_ analysera ce dossier au démarrage, en copiant sa structure de dossiers dans le dossier personnalisé sans les fichiers.
- `<CustomDirLocation>` : Spécifie l'emplacement du dossier personnalisé. C'est dans ce dossier que les raccourcis ajoutés par l'utilisateur sont stockés.

### Configuration de la police

- `<FontSize>18</FontSize>`: Change la taille de police des labels des raccourcis.
- `<PieItemLabelColor>#FFFFFF</PieItemLabelColor>`: Change la couleur de la police des labels des raccourcis. La couleur doit être en format hexadécimal.

### Consentement de l'utilisateur

`<UserConsent>` : Représente le drapeau de consentement de l'utilisateur. Utilisé avec l'option de lancement `--check-startup-config`.

### Icône centrale changeante

`<ChangingCenterIcon>` : Définis si l'icône centrale doit changer pour celle du sous dossier exploré ou si elle doit être statique. 

### Raccourci d'invocation alternatif

`<AltHotkey>` : Définis si le raccourci d'invocation alternatif (`Ctrl + Win + Alt gauche`) doit être utilisé pour remplacer le raccourci standard (`Ctrl + Win + Maj gauche`)

### Notes sur le fichier de configuration

- Les changements prendront effet lors du prochain démarrage de l'application.
- Les options avec une valeur décimale doivent utiliser une virgule (,) comme séparateur décimal.
- Le fichier de configuration se trouve dans le fichier `config/config.xml`.
- Toute option non présente dans cette documentation (qui pourrait être ajoutée par _OvalDock_) n'est pas prise en charge et sera ignorée.
- Toute valeur ou chemin de fichier non valide entraîne un comportement indéfini, souvent précédé d'un message d'avertissement et suivi de l'utilisation de valeurs par défaut arbitraires.
