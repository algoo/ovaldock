﻿# _OvalDock_ User Documentation

_OvalDock_ is a dock software designed for Windows 7 to 11. It is positioned on the main screen and provides
shortcuts and virtual folders of shortcuts. This user documentation will guide you through the features and usage of _OvalDock_.

## Installation

To install _OvalDock_, follow these steps:

1. Extract the _OvalDock_ files to a desired folder on your computer.
2. Launch the `ovaldock.exe` file from the extracted folder.

It might be required to install the redistribuables of the proper architecture
located at `./redistribuables` before starting _OvalDock_

## Usage

### Dock Interface

The _OvalDock_ interface consists of shortcuts and virtual folders.

- To launch a shortcut, click on it.
- To navigate through a virtual folder, click on it.
- To go back, click on the center image of the dock.

### Folder Parsing

During boot, _OvalDock_ parses two folders: protected and custom, which are specified in the configuration file. (see below)
The parsing process starts with the protected folder, copying its folder structure without the files to the custom folder.
This process does not overwrites the contents of the custom folder in the event of a folder already exists.

Hidden files and folders are ignored during parsing.
If a file with the same name and position as a protected file exists in the custom folder, the custom one is ignored.

### Adding Shortcuts

You can add shortcuts to _OvalDock_ by dragging and dropping files onto the dock. The shortcut will be added to the current folder.
This action creates in the custom folder a new shortcut that points to the dropped file.

### Context Menu

Right-clicking on the dock or a shortcut in the custom folder opens a context menu with various options:

#### Dock Context Menu

- Refresh: Reparses the protected and custom folders to display manual modifications.
- Dock Settings: Opens a visual settings menu for _OvalDock_.
- Hide: Hides the dock from view.
- Quit: Shuts down _OvalDock_.
- Reset: Erases the content of the custom folder.

#### Shortcut Context Menu

Right-clicking on a shortcut in the custom folder opens a context menu with two options: Remove and Rename.
Selecting the Remove option sends the shortcut to the trash bin.

#### Tray icon context menu

_OvalDock_ also features a tray icon located in the system tray. Right-clicking on the tray icon opens a context menu with the following options:

- Dock Settings: Opens a visual settings menu for _OvalDock_.
- Hide / Show: Toggles the visibility of the dock.
- Quit: Shuts down _OvalDock_.

### Dock Settings
_OvalDock_ provides a settings window where you can customize various aspects of the dock. The available options include:

- Center Icon size
- Center Icon Opacity
- Disk Radius
- Disk Opacity
- Icons Size
- Icons Opacity
- Icons distance from center
- Font size and color
- User consent

### Keyboard Shortcuts

To bring _OvalDock_ back to the front or un-hide it, use the keyboard shortcut `Ctrl + Windows + Shift`.

## Launch Options

_OvalDock_ accepts a launch option `--check-startup-config` with the following behavior:

- If the `UserConsent` field in the configuration file is set to `false`, _OvalDock_ shuts down immediately.
- If the `UserConsent` field is set to `true`, _OvalDock_ starts normally.
- If the `UserConsent` field is not present, _OvalDock_ asks for user consent and stores the answer in the `UserConsent` field.
- To prompt the consent again, delete the UserConsent field from the configuration file.
- Omitting this launch option will ignore consent and start _OvalDock_ normally.

## Accessibility

_OvalDock_ is designed with accessibility in mind to ensure a smooth user experience for individuals with diverse needs. Here are the accessibility features available in _OvalDock_:

### Keyboard Navigation

_OvalDock_ supports full keyboard usage, allowing users to navigate through the interface using the Tab key.
- Using the Tab key moves the focus between shortcuts and virtual folders.
- Pressing the Enter or Space key activates the selected item (navigating / launching shortcuts).

### Screen Readers

_OvalDock_ is compatible with screen reader software, making it accessible to individuals who rely on auditory feedback. The following screen readers are supported:

- Windows Narrator: _OvalDock_ is fully compatible with Windows Narrator, enabling users to navigate and interact with the dock using the screen reader.
- NVDA Narrator: _OvalDock_ supports NVDA Narrator, ensuring accessibility for users who prefer this screen reader.

## Caveats

- _OvalDock_ cannot launch `.exe` files directly but can launch shortcuts of `.exe` files. I.E. adding an `.exe` file
by putting it in the `custom` or `protected` directories is not supported
- If _OvalDock_ size or placement is incorrect, often following a change of primary screen,
  pressing `Ctrl + Win + Shift` will readjust it's position and size
- The performance and visual aspect of _OvalDock_ depend on your computer's capabilities.
  As the number of shortcuts in a virtual folder increases, they may become closely positioned,
  making it harder to differentiate between them.


## Configuration File Documentation

The configuration file for _OvalDock_ contains various settings that control the appearance and behavior of the dock. 
This documentation will guide you through the different elements of the configuration file and their respective purposes.

The configuration file follows an XML structure with a root element `<Root>`. Inside the root element, you will find several child elements representing different configuration settings.

At startup __OvalDock__ will check if the file `%appdata%/OvalDock/config.xml` exists, if it doesn't, it will copy
`./config/config.xml` vers `%appdata%/OvalDock/config.xml`, this file will now be used as the user's configuration file.

### Dock Position

- `<WindowPositionPercentageWidth>`: Sets the position of the center of the dock as a percentage along the X-axis of the main screen.
- `<WindowPositionPercentageHeight>`: Sets the position of the center of the dock  as a percentage along the Y-axis of the main screen.

### Inner Disk Configuration

- `<InnerDiskImagePath>`: Specifies the file path for the inner disk image. The image will be displayed at the center of the dock.
- `<InnerRadius>`: Sets the radius of the inner disk.
- `<InnerDiskNormalOpacity>`: Defines the opacity level of the inner disk. (0,0 - 1,0)

### Outer Disk Configuration
- `<OuterDiskImagePath>`: Specifies the file path for the outer disk image.
- `<OuterRadius>`: Sets the radius of the outer disk.
- `<OuterDiskNormalOpacity>`: Defines the opacity level of the outer disk. (0,0 - 1,0)

### Icon Configuration
- `<FileNotFoundIcon>`: Specifies the file path for the icon displayed when a file is not found or inaccessible.
- `<FolderDefaultIcon>`: Sets the default icon for folders in the dock.

### Pie Item Configuration
- `<PieItemSize>`: Specifies the size of each pie item (shortcut) displayed in the dock.
- `<PieItemNormalOpacity>`: Defines the opacity level of the pie items (shortcuts). (0,0 - 1,0)
- `<PieItemRadiusFromCenter>`: Sets the distance of the pie items (shortcuts) from the center of the dock.

### Folder Locations
- `<ProtectedDirLocation>`: Specifies the location of the protected folder. _OvalDock_ will parse this folder during startup, copying its folder structure to the custom folder without the files.
- `<CustomDirLocation>`: Specifies the location of the custom folder. This is the folder where user-added shortcuts are stored.

Both paths can use environment variables.

### Font configuration

- `<FontSize>18</FontSize>`: Changes the font size of the labels
- `<PieItemLabelColor>#FFFFFF</PieItemLabelColor>`: Changes the font color of the labels. The color must be in hexadecimal format.

### User Consent
`<UserConsent>`: Represents the user consent flag. Used with the `--check-startup-config` launch option.

### Changing center icon

`<ChangingCenterIcon>` : Defines if the central icon must change to the current subfolder icon or if it must stay static.

### Alternative hotkey

`<AltHotkey>` : Defines if the alternative hotkey (`Ctrl + Win + Left Alt`) must be used to replace the standard hotkey (`Ctrl + Win + Left Shift`)

### Notes about the configuration file
- The changes will take effect upon the next startup of the application.
- Options featuring a decimal value must use a comma (,) as a decimal separator.
- The configuration file is located in `config/config.xml` file.
- Any option not present in this documentation (that might be added by OvalDock) is not supported and will be ignored.
- Any invalid value or file path follows an undefined behavior, often preceded by a warning message and followed by the usage of arbitrary default values.
