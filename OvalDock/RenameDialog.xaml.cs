﻿using System.Windows;

namespace OvalDock
{
	public partial class RenameDialog : Window
	{
		public RenameDialog()
		{
			InitializeComponent();
		}
		
		public string UserInput { get; private set; }

		public RenameDialog(string prompt, string val)
		{
			InitializeComponent();
			PromptTextBlock.Text = prompt;
			InputTextBox.Text = val;
		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			UserInput = InputTextBox.Text;
			DialogResult = true;
		}

		private void CancelButton_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = false;
		}
	}
}
