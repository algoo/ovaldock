﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OvalDock
{
    class Win32Helper
    {
        // COM Interop Declarations
    [ComImport]
    [Guid("00021401-0000-0000-C000-000000000046")]
    internal class ShellLink
    {
    }

    [ComImport]
    [Guid("000214F9-0000-0000-C000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IShellLinkW
    {
        void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] out string pszFile, int cchMaxPath, IntPtr pfd, uint fFlags);
        void GetIDList(out IntPtr ppidl);
        void SetIDList(IntPtr pidl);
        void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] out string pszName, int cchMaxName);
        void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
        void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] out string pszDir, int cchMaxPath);
        void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
        void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] out string pszArgs, int cchMaxPath);
        void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
        void GetHotkey(out ushort pwHotkey);
        void SetHotkey(ushort wHotkey);
        void GetShowCmd(out int piShowCmd);
        void SetShowCmd(int iShowCmd);
        void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] out string pszIconPath, int cchIconPath, out int piIcon);
        void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
        void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, uint dwReserved);
        void Resolve(IntPtr hwnd, uint fFlags);
        void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
    }

    [ComImport]
    [Guid("0000010b-0000-0000-C000-000000000046")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    internal interface IPersistFile
    {
        void GetClassID(out Guid pClassID);
        void IsDirty();
        void Load([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, uint dwMode);
        void Save([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, [MarshalAs(UnmanagedType.Bool)] bool fRemember);
        void SaveCompleted([MarshalAs(UnmanagedType.LPWStr)] string pszFileName);
        void GetCurFile([MarshalAs(UnmanagedType.LPWStr)] out string ppszFileName);
    }
    
    public static void CreateShortcut(string targetPath, string shortcutPath)
    {
        try
        {
            // Create a shell link object
            var shellLink = (IShellLinkW)new ShellLink();

            // Set the target path for the shortcut
            shellLink.SetPath(targetPath);

            // Set working directory, description, and icon location
            shellLink.SetWorkingDirectory(Path.GetDirectoryName(targetPath));
            shellLink.SetDescription(Path.GetFileNameWithoutExtension(targetPath));

            // Create a persist file object to save the shortcut
            var persistFile = (IPersistFile)shellLink;
            persistFile.Save(shortcutPath, false);
        }
        catch (Exception ex)
        {
            MessageBox.Show("Une erreur est survenue lors de la création du raccourcis " + ex.Message);
        }
    }
        
        [ComImportAttribute()]
        [GuidAttribute("46EB5926-582E-4017-9FDF-E8998DAA0950")]
        [InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
        private interface IImageList
        {
            [PreserveSig]
            int Add(
                IntPtr hbmImage,
                IntPtr hbmMask,
                ref int pi);

            [PreserveSig]
            int ReplaceIcon(
                int i,
                IntPtr hicon,
                ref int pi);

            [PreserveSig]
            int SetOverlayImage(
                int iImage,
                int iOverlay);

            [PreserveSig]
            int Replace(
                int i,
                IntPtr hbmImage,
                IntPtr hbmMask);

            [PreserveSig]
            int AddMasked(
                IntPtr hbmImage,
                int crMask,
                ref int pi);

            [PreserveSig]
            int Draw(
                ref IMAGELISTDRAWPARAMS pimldp);

            [PreserveSig]
            int Remove(
                int i);

            [PreserveSig]
            int GetIcon(
                int i,
                int flags,
                ref IntPtr picon);
        };
        private struct IMAGELISTDRAWPARAMS
        {
            public int cbSize;
            public IntPtr himl;
            public int i;
            public IntPtr hdcDst;
            public int x;
            public int y;
            public int cx;
            public int cy;
            public int xBitmap;
            public int yBitmap;
            public int rgbBk;
            public int rgbFg;
            public int fStyle;
            public int dwRop;
            public int fState;
            public int Frame;
            public int crEffect;
        }
        private struct SHFILEINFO
        {
            public IntPtr hIcon;
            public int iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 254)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szTypeName;
        }

        private const int SHGFI_SYSSMALL = 0x3;
        private const int SHGFI_SMALLICON = 0x1;
        private const int SHGFI_LARGEICON = 0x0;
        private const int SHIL_JUMBO = 0x4;
        private const int SHIL_EXTRALARGE = 0x2;
        private const int WM_CLOSE = 0x0010;

        public enum IconSizeEnum
        {
            SysSmall = SHGFI_SYSSMALL,
            SmallIcon16 = SHGFI_SMALLICON,
            MediumIcon32 = SHGFI_LARGEICON,
            LargeIcon48 = SHIL_EXTRALARGE,
            ExtraLargeIcon = SHIL_JUMBO
        }

        [DllImport("user32")]
        private static extern
            IntPtr SendMessage(
                IntPtr handle,
                int Msg,
                IntPtr wParam,
                IntPtr lParam);

        [DllImport("shell32.dll")]
        private static extern int SHGetImageList(
            int iImageList,
            ref Guid riid,
            out IImageList ppv);

        [DllImport("Shell32.dll")]
        private static extern int SHGetFileInfo(
            string pszPath,
            int dwFileAttributes,
            ref SHFILEINFO psfi,
            int cbFileInfo,
            uint uFlags);

        [DllImport("user32")]
        private static extern int DestroyIcon(
            IntPtr hIcon);
        
        //Modifiers:
        public const uint MOD_NONE = 0x0000;
        public const uint MOD_ALT = 0x0001;
        public const uint MOD_CONTROL = 0x0002;
        public const uint MOD_SHIFT = 0x0004;
        public const uint MOD_WIN = 0x0008;

        public const uint VK_CAPITAL = 0x14;
        public const uint VK_LWIN = 0x5B;
        public const uint VK_SHIFT = 0x10;
        public const uint VK_CONTROL = 0x11;
        public const uint VK_MENU = 0x12;

        private static double GetTransparentPixelsPercentage(Icon refIcon)
        {
            // On crée un BitmapSource à partir de l'icône. (Cela permet de manipuler les pixels de l'icône)
            BitmapSource bitmap = Imaging.CreateBitmapSourceFromHIcon(
                refIcon.Handle,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());
            
            // On convertit le format de l'image en Bgra32 si ce n'est pas déjà le cas
            // (Pour garantir une uniformité dans le traitement des pixels)
            if (bitmap.Format != PixelFormats.Bgra32)
            {
                bitmap = new FormatConvertedBitmap(bitmap, PixelFormats.Bgra32, null, 0);
            }

            // On récupère les pixels de l'image dans un tableau de bytes
            // (chaque pixel est représenté par 4 bytes : B, G, R, A)
            var pixelCount = bitmap.PixelWidth * bitmap.PixelHeight;
            var pixels = new byte[4 * pixelCount];

            bitmap.CopyPixels(pixels, 4 * bitmap.PixelWidth, 0);

            var transparentPixelCount = 0;

            // On compte le nombre de pixels transparents
            // (On commence à l'indice 3 pour accéder aux valeurs d'alpha)
            for (var i = 3; i < 4 * pixelCount; i += 4) // start at first alpha value
            {
                if (pixels[i] == 0)
                {
                    transparentPixelCount++;
                }
            }

            return (double)transparentPixelCount / pixelCount;
        }
        
        // Cette fonction prend un chemin de fichier et retourne l'icône associée 
        // de la meilleure qualité possible au format bitmap.
        public static Icon GetBestIcon(string filepath)
        {
            // On récupère l'icône de référence avec la fonction GetIcon.
            // (La taille 32x32 est la seule taille qui doit obligatoirement exister et être bien formatée si une icône est présente.)
            Icon refIcon = GetIcon(filepath, IconSizeEnum.MediumIcon32);

            // On calcule le pourcentage de pixels transparents dans l'icône de référence.
            double refPercentage = GetTransparentPixelsPercentage(refIcon);

            // On prépare toutes les tailles d'icônes que l'on souhaite récupérer (taille max, 48px, 32px).
            IconSizeEnum[] sizes = { IconSizeEnum.ExtraLargeIcon, IconSizeEnum.LargeIcon48, IconSizeEnum.MediumIcon32 };

            // Pour chaque taille souhaitée (dans l'ordre)
            foreach (var iconSize in sizes.Select((value, i) => new { i, value }))
            {
                // On récupère l'icône correspondante.
                Icon candidateIcon = GetIcon(filepath, iconSize.value);
                // On calcule le pourcentage de pixels transparents.
                double candidatePercentage = GetTransparentPixelsPercentage(candidateIcon);
                // On compare le pourcentage de l'icône candidate avec celui de l'icône de référence avec une marge de 30 % 
                // (pour tenir compte des variations, de l'antialiasing, etc.)
                if (candidatePercentage <= refPercentage * 1.3)
                    // Si l'icône candidate semble correcte, on la renvoie.
                    return candidateIcon;
            }

            // Si aucune meilleure icône n'est trouvée, on renvoie l'icône de référence.
            return refIcon;
        }


        // Cette fonction prend un chemin de fichier et une taille d'icône, 
        // et renvoie l'icône correspondante.
        public static Icon GetIcon(string filepath, IconSizeEnum iconsize)
        {
            IntPtr hIcon = IntPtr.Zero;
            if (System.IO.Directory.Exists(filepath))
                hIcon = GetIconHandleFromFolderPath(filepath, iconsize);
            else
            {
                if (System.IO.File.Exists(filepath))
                    hIcon = GetIconHandleFromFilePath(filepath, iconsize);
            }
            return GetBitmapFromIconHandle(hIcon);
        }
    
        // Ces deux fonctions sont des wrappers de la fonction GetIconHandleFromFilePathWithFlags 
        // qui appliquent les flags appropriés selon si le chemin pointe vers un fichier ou un dossier.
        private static IntPtr GetIconHandleFromFilePath(string filepath, IconSizeEnum iconsize)
        {
            var shinfo = new SHFILEINFO();
            const uint SHGFI_SYSICONINDEX = 0x4000;
            const int FILE_ATTRIBUTE_NORMAL = 0x80;
            uint flags = SHGFI_SYSICONINDEX;
            return GetIconHandleFromFilePathWithFlags(filepath, iconsize, ref shinfo, FILE_ATTRIBUTE_NORMAL, flags);
        }
        
        private static IntPtr GetIconHandleFromFolderPath(string folderpath, IconSizeEnum iconsize)
        {
            var shinfo = new SHFILEINFO();
        
            const int FILE_ATTRIBUTE_DIRECTORY = 0x00000010;
            const uint SHGFI_SYSICONINDEX = 0x4000;
            uint flags = SHGFI_SYSICONINDEX;
            return GetIconHandleFromFilePathWithFlags(folderpath, iconsize, ref shinfo, FILE_ATTRIBUTE_DIRECTORY, flags);
        }

        // Cette fonction convertit le pointeur d'icône système en une icône bitmap, 
        // en libérant les ressources associées au pointeur source.
        private static Icon GetBitmapFromIconHandle(IntPtr hIcon)
        {
            // Sécurité au cas où le pointeur soit nul.
            if (hIcon == IntPtr.Zero) return null;
            // On récupère une copie de l'icône pointée.
            Icon myIcon = (Icon)Icon.FromHandle(hIcon).Clone();
            // On libère le pointeur d'origine qui n'est pas géré par le garbage collector (car pointeur système).
            DestroyIcon(hIcon);
            // On ferme le handle associée à l'icône.
            SendMessage(hIcon, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            return myIcon;
        }

        // Cette fonction récupère un pointeur vers un handle de l'icône de l'élément et le retourne.
        private static IntPtr GetIconHandleFromFilePathWithFlags(string filepath, IconSizeEnum iconsize, ref SHFILEINFO shinfo, int fileAttributeFlag, uint flags)
        {
            const int ILD_TRANSPARENT = 1;
            // On récupère les métadonnées de l'élément désiré.
            var retval = SHGetFileInfo(filepath, fileAttributeFlag, ref shinfo, Marshal.SizeOf(shinfo), flags);
            if (retval == 0) throw (new System.IO.FileNotFoundException());
            // On récupère les informations liées aux icônes (liste d'icônes, présente au GUID ci-dessous).
            var iconIndex = shinfo.iIcon;
            var iImageListGuid = new Guid("46EB5926-582E-4017-9FDF-E8998DAA0950");
            IImageList iml;
            // On récupère une référence vers la taille d'icône désirée.
            var hres = SHGetImageList((int)iconsize, ref iImageListGuid, out iml);
            // On crée un pointeur vide pour recevoir l'adresse de l'handle de l'icône désirée.
            var hIcon = IntPtr.Zero;
            hres = iml.GetIcon(iconIndex, ILD_TRANSPARENT, ref hIcon);
            // On retourne le pointeur.
            return hIcon;
        }

        private static void SafeDelete(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch ( IOException ) {}
        }

        
        public delegate void WrapSharingViolationsCallback();
        
        public static bool WaitForFile (WrapSharingViolationsCallback callback)
        {
            for (int numTries = 0; numTries < 20; numTries++) {
                try
                {
                    callback();
                    return true;
                }
                catch (IOException) {
                    Thread.Sleep (50);
                }
            }

            return false;
        }
        
        [DllImport("user32.dll")]
        static extern bool SetWindowPos(
            IntPtr hWnd,
            IntPtr hWndInsertAfter,
            int X,
            int Y,
            int cx,
            int cy,
            uint uFlags);

        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;

        static readonly IntPtr HWND_BOTTOM = new IntPtr(1);

        public static void SendWpfWindowBack(Window window)
        {
            var hWnd = new WindowInteropHelper(window).Handle;
            SetWindowPos(hWnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
        }
    }

    class LowWin32Helper
    {
        //Modifiers:
        public const uint MOD_NONE = 0x0000;
        public const uint MOD_ALT = 0x0001;
        public const uint MOD_CONTROL = 0x0002;
        public const uint MOD_SHIFT = 0x0004;
        public const uint MOD_WIN = 0x0008;

        public const uint VK_CAPITAL = 0x14;
        public const uint VK_LWIN = 0x5B;
        
        // Method to get the system icon path
        public static string GetSystemIconPath(string filePath)
        {
            SHFILEINFO shinfo = new SHFILEINFO();
            IntPtr result = SHGetFileInfo(filePath, 0, ref shinfo, (uint)Marshal.SizeOf(shinfo), SHGFI_ICON | SHGFI_SMALLICON);

            if (result != IntPtr.Zero)
            {
                // Extract the icon path from the display name (szDisplayName) property
                int index = shinfo.szDisplayName.IndexOf(",0", StringComparison.Ordinal);
                if (index >= 0)
                {
                    return shinfo.szDisplayName.Substring(0, index);
                }
            }

            return null; // Failed to get the icon path
        }
        
        public static bool ContainsNonPrintableCharacters(string input)
        {
            // Regular expression pattern to match non-printable characters
            string pattern = @"[^\x20-\x7E\t\n\r]";

            // Check if the input string contains any matches of the pattern
            return Regex.IsMatch(input, pattern);
        }
        
        // Method to get the custom folder icon path
        public static string GetCustomFolderIconPath(string folderPath)
        {
            string desktopIniPath = Path.Combine(folderPath, "desktop.ini");

            if (File.Exists(desktopIniPath))
            {
                string truePath = null;
                List<string> fileLines = File.ReadAllLines(desktopIniPath).ToList();
                bool hasPath = false;
                fileLines.ForEach(line =>
                {
                    if (line.StartsWith("IconResource"))
                    {
                        truePath = line["IconResource=".Length..];
                        
                        hasPath = true;
                        if (truePath.EndsWith(",0"))
                        {
                            truePath = truePath.Substring(0, truePath.Length - ",0".Length);
                        }
                    }
                });
                if (hasPath)
                    return truePath;
            }

            return null; // Failed to get the custom folder icon path
        }

        // Stuff copy pasted from stackoverflow,
        // https://stackoverflow.com/questions/42910628/is-there-a-way-to-get-the-windows-default-folder-icon-using-c
        // modified to get a specific folder's icon rather than the stock one,
        // but also to properly destroy the handle afterwards
        public static Icon GetIcon(string path)
        {
            SHFILEINFO shinfo = new SHFILEINFO();
            SHGetFileInfo(
                path,
                0, ref shinfo, (uint)Marshal.SizeOf(shinfo),
                SHGFI_ICON | SHGFI_LARGEICON);
            
            Icon result = (Icon)Icon.FromHandle(shinfo.hIcon).Clone(); // Get a copy that doesn't use the original handle
            DestroyIcon(shinfo.hIcon); // Clean up native icon to prevent resource leak

            return result;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct SHFILEINFO
        {
            public IntPtr hIcon;
            public int iIcon;
            public uint dwAttributes;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string szDisplayName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
            public string szTypeName;
        };

        [DllImport("user32.dll")]
        public static extern bool DestroyIcon(IntPtr handle);
                
        [DllImport("shell32.dll")]
        private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

        private const uint SHSIID_FOLDER = 0x3;
        private const uint SHGSI_ICON = 0x100;
        private const uint SHGSI_LARGEICON = 0x0;
        private const uint SHGSI_SMALLICON = 0x1;

        private const uint SHGFI_ICON = 0x100;
        private const uint SHGFI_LARGEICON = 0x0;
        private const uint SHGFI_SMALLICON = 0x000000001;
    }
}
