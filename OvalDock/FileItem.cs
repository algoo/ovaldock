﻿using System;
using System.Collections.Generic;

using System.Windows;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;

namespace OvalDock
{
    // Used for loading the settings window.
    // TODO: Could probably use for other things too.
    public enum FileItemType
    {
        File,
        Folder,
        Other
    }

    class FileItem : PieItem
    {
        public FileItemType Type { get; private set; }

        public string Arguments { get; private set; }

        public override CachedImage Icon
        {
            get
            {
                // Check to make sure we have a BitmapSource cached.
                if (base.Icon.ImageBitmapSource != null)
                    return base.Icon;

                // No cache. Different cache needed to be generated based on type.
                string truePath = FilePath;
                switch(Type)
                {
                    case FileItemType.File:
                        // Extract the file icon as a BitmapSource otherwise.
                        // TODO: Just copy pasted this code from somewhere.
                        try
                        {
                            if (Path.GetExtension(FilePath) == ".url")
                            {
                                List<string> fileLines = File.ReadAllLines(FilePath).ToList();
                                fileLines.ForEach(line =>
                                {
                                    if (line.StartsWith("IconFile="))
                                    {
                                        truePath = line["IconFile=".Length..];
                                    }
                                });
                            }
                            System.Drawing.Icon sysicon = Win32Helper.GetBestIcon(truePath);
                            //var bmpSrc = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(
                            //            sysicon.Handle,
                            //            System.Windows.Int32Rect.Empty,
                            //            System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
                            //sysicon.Dispose();

                            // Icon extracted. Save for later and return.
                            base.Icon.ImageBitmap = sysicon.ToBitmap();
                            return base.Icon;
                        }
                        catch (Exception)
                        {
                            //MessageBox.Show(e.Message);
                            try
                            {
                                LowWin32Helper.GetIcon(truePath);
                            }
                            catch
                            {
                                SetFileNotFoundIcon();
                            }
                            return base.Icon;
                        }

                    case FileItemType.Folder:
                        if(!Directory.Exists(FilePath))
                        {
                            SetFileNotFoundIcon();
                            return base.Icon;
                        }

                        System.Drawing.Icon icon = Win32Helper.GetBestIcon(FilePath);
                        try
                        {
                            icon = Win32Helper.GetBestIcon(FilePath);
                        }
                        catch (Exception e)
                        {
                             MessageBox.Show(e.Message);
                        }
                        // TODO: I really hope this method never breaks for some stupid reason.

                        // Possibly unnecessary.
                        base.Icon.ClearCache();
                        base.Icon.ImagePath = null;

                        base.Icon.ImageBitmap = icon.ToBitmap();
                        return base.Icon;

                    case FileItemType.Other:
                        SetFileNotFoundIcon();
                        return base.Icon;

                    default: // This should never happen
                        SetFileNotFoundIcon();
                        return base.Icon;
                }

                
            }
        }

        private void SetFileNotFoundIcon()
        {
            // Icon could not be extracted. Use the "file not found" icon then.
            // TODO: The default CAN be null? What happens then?
            //
            // Clone because we CAN modify the icon directly later.
            // Load cache on main copy beforehand to do as little work on the rest of the copies as possible.
            //
            // TODO: This whole cloning technique is not great.
            //       There is no way to conveniently check if we are using a "file not found" icon.
            Config.FileNotFoundIcon.CreateCache();
            base.Icon = Config.FileNotFoundIcon.Copy();
        }

        public override string Name
        {
            get
            {
                if (IsCustomName)
                    return base.Name;
                else
                {
                    try
                    {
                        return Path.GetFileName(FilePath);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
            }
        }

        public const string TYPE_NAME = "FileItem";
        public override string TypeName { get { return TYPE_NAME; } }

        public FileItem(bool customName, string label, bool customIcon, string iconPath, string filePath, string arguments, FileItemType type, bool isProtected = false) : base(customName, label, customIcon, iconPath, filePath, isProtected)
        {
            FilePath = filePath;
            Arguments = arguments;
            Type = type;

            //TODO: Handle the file not existing somehow?
        }

        public override void LeftClick(MainWindow mainWindow)
        {
            if (Type == FileItemType.File && !File.Exists(FilePath))
            {
                MessageBox.Show("File does not exist.");
                return;
            }

            if(Type == FileItemType.Folder && !Directory.Exists(FilePath))
            {
                MessageBox.Show("Folder does not exist.");
                return;
            }

            ProcessStartInfo processStartInfo = new ProcessStartInfo();

            processStartInfo.FileName = FilePath;
            processStartInfo.Arguments = Arguments;

            // TODO: Are there other executable file types? .msi? .com?
            if (File.Exists(FilePath) && FilePath.EndsWith(".exe"))
            {
                string workingDirectory = Path.GetDirectoryName(FilePath);

                if(workingDirectory != null && workingDirectory != "")
                {
                    processStartInfo.WorkingDirectory = workingDirectory;
                }
            }
            else
            {
                // ProcessStartInfo.UseShellExecute Property
                // true if the shell should be used when starting the process;
                // false if the process should be created directly from the executable file.
                // The default is true on .NET Framework apps and false on .NET Core apps.
                processStartInfo.UseShellExecute = true;
            }

            try
            {
                if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return;
                Process.Start(processStartInfo);
                Win32Helper.SendWpfWindowBack(mainWindow);
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur est survenue avec le raccourci");
            }           
        }

        public override void SaveConfig(XmlElement element)
        {
            base.SaveConfig(element);

            element.SetAttribute("FilePath", FilePath == null ? "" : FilePath);
            element.SetAttribute("Arguments", Arguments == null ? "" : Arguments);

            switch(Type)
            {
                case FileItemType.File:
                    element.SetAttribute("Type", "File");
                    break;

                case FileItemType.Folder:
                    element.SetAttribute("Type", "Folder");
                    break;

                case FileItemType.Other:
                    element.SetAttribute("Type", "Other");
                    break;
            }
        }

        public override void LoadConfig(XmlElement element)
        {
            base.LoadConfig(element);

            FilePath = element.GetAttribute("FilePath");
            Arguments = element.GetAttribute("Arguments");

            switch(element.GetAttribute("Type"))
            {
                case "File":
                    Type = FileItemType.File;
                    break;

                case "Folder":
                    Type = FileItemType.Folder;
                    break;

                case "Other":
                    Type = FileItemType.Other;
                    break;
            }
        }
    }
}
